#!/usr/bin/python3

import matplotlib.pyplot as plt
import numpy as np
import os
import random

def crea_img(indice):
    # creo array dalla stringa
    a = np.array(list(indice), dtype=int)
    # lo riarrangio 5x3
    a = np.reshape(a,(5,3))
    # aggiungo due colonne mirrorando le prime due
    a = np.hstack((a,a[:,1].reshape(5,1)))
    a = np.hstack((a,a[:,0].reshape(5,1)))

    print("Genero " + indice + ".eps")
    img = plt.imshow(a, interpolation='nearest')
    img.set_cmap('hot')
    plt.axis('off')
    #plt.savefig(indice + ".png", bbox_inches='tight')
    plt.savefig(indice + ".eps", format="eps", bbox_inches='tight')

while input('Creo immaginuzza? (y/n):  ') == 'y':
    i = format(random.randrange(32767),'015b')
    if os.path.isfile(i + ".eps"):
        print("Esiste già")
        continue
    else:
        crea_img(i)

print("Finito.")
